//Imports the needed modules
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const postCssPresetEnv = require('postcss-preset-env');
const postCssScss = require('postcss');

module.exports = {
    //Set entries for JS and SCSS  (point where to start the application bundling process)
    entry: [
        __dirname + '/src/js/app.js',
        __dirname + '/src/scss/app.scss',
    ],
    output: {
        // set absolute path for the output directory
        path: path.resolve(__dirname, 'dist'),
        // set name of the generated JS file
        filename: 'js/app.min.js',
    },
    module: {
        rules: [
            // rule to detect scss files to compile them into css and minify the generated file
          {
            test: /\.(sa|sc|c)ss$/,
            exclude: ['/node_modules'],
            use: [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  url: false,
                  //minimize: process.env.NODE_ENV === 'production',
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  sourceMap: true,
                  //syntax: postCssScss,
                  /*plugins: () => [
                    autoprefixer,
                    postCssPresetEnv({
                      stage: 0,
                      features: {
                        'color-mod-function': true,
                        'alpha-hex-colors': true
                      }
                    }),
                  ],*/
                },
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true
                }
              }
            ]
          },
            {
              test: require.resolve('jquery'),
              loader: 'expose-loader',
              options: {
                exposes: ['$', 'jQuery'],
              },
            },
        ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'css/app.min.css',
      }),
      // CopyPlugin will copy files that are matching with listed patterns
      new CopyPlugin({
        patterns: [
          // -> Copying folder front from src folder to dist with optimizing svg fonts
          // -> Copying folder images from src folder to dist with optimizing all images
         { from: path.resolve(__dirname, 'src/images'), to: 'images' },
          { from: path.resolve(__dirname, 'src/fonts'), to: 'fonts' },
          { from: path.resolve(__dirname, 'src/js/vendor/jquery/jquery.js'), to: 'js/jquery.min.js' }

        ],
        options: {
          // Number of simultaneous requests
          concurrency: 100,
        },
      }),
      // Imagemin will compress all images files(jpe?g|png|gif|svg)
      new ImageminPlugin({}),
      new webpack.ProvidePlugin({
        $: path.resolve(path.join(__dirname, 'node_modules/jquery')),
        jQuery: path.resolve(path.join(__dirname, 'node_modules/jquery')),
        'window.jQuery': path.resolve(path.join(__dirname, 'node_modules/jquery')),
      })
    ],
    performance: { hints: false }, // To disable warning message (it appears when the minified file surpass the recommended size limits)
};
