const Carousels = () => {
  $( ".owl-carousel" ).each(function() {
    var carousel = $(this).attr('id');
    var carouselId = '#'+ carousel ;
    var MobileItem = $(this).attr('data-mobile');
    var DesktopItem = $(this).attr('data-desktop');
    $(carouselId).owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      center : true,
      responsive: {
        0: {
          items: MobileItem
        },
        768: {
          items: DesktopItem
        }
      }
    })
  });
}
export { Carousels }
