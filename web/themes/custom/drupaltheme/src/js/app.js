// Import dependencies
require('jquery/dist/jquery.min.js')
require('popper.js/dist/popper')
require('bootstrap/dist/js/bootstrap')
require('owl.carousel/dist/owl.carousel.min')
const AOS = require('aos/dist/aos')

// Import components
import { Sliders } from './components/Sliders'
import { Carousels } from "./components/Carousel";
import { CounterNumber } from "./components/Counter";


// AOS
AOS.init()

jQuery(document).ready(function($) {

// Sliders
  Sliders()
  //Carousels
  Carousels()
  // Number counter
  CounterNumber()
})
