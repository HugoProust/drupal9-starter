# Webpack
This theme is using webpack to generate assets(css/js/images/fonts)
## Setup
    cd /path/web/themes/custom/yourthemename
    npm install
## Usage
The SCSS files are in `src/scss` and JavaScript files are in `src/js` and images files are in `src/images` and fonts files are in `src/fonts` directories.

    cd /path/web/themes/custom/yourthemename
    npm run build

To start webpack's watch mode

    npm run start
